require('dotenv').config();
const baseUrl = 'http://localhost:8000'
const environment =  {
    baseUrl,
    debug: true
}

export default  environment;