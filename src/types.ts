import React from 'react'

export type ChangeEventType = React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>

export interface IProject { id: number; name: string }
export interface IProjectReq { name: string }
export interface ILink { id: number; link: string; project: number }
export interface ILinkReq { link: string; project: number }
export interface IInformation { id: number; mail: string; password: string; project: number }
export interface IInformationReq { mail: string; password: string; project: number }
export interface IUser { id: number; username: string; email: string; first_name: string; last_name: string; }

export interface IAction<Type> {
    type: string;
    payload: Type;
}

export interface IActionType {
    type: string;
}