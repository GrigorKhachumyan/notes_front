import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { Box, Button, Toolbar, AppBar } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    title: {
      flexGrow: 1,
    },
  })
);

export default function ButtonAppBar() {
  const classes = useStyles();
  const history = useHistory();

  return (
    <Box className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Button onClick={() => history.push("/")} color="inherit">
            Home
          </Button>
          <Box className={classes.title}></Box>
          <Button onClick={() => history.push("Login")} color="inherit">
            Login
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
