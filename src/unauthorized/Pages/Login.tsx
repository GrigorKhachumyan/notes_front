import React, { useState, KeyboardEventHandler } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import { useSelector, useDispatch } from "../../hooks";

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";

import { ChangeEventType } from "../../types";

import { requestUser } from "../../redux/sagas/login/actions";
import {
  setUserName,
  setUserPassword,
} from "../../redux/reducers/login/actions";

export default function Login() {
  const history = useHistory();

  const dispatch = useDispatch();

  const loginInfo = useSelector((state) => {
    return state.user.loginInfo;
  });

  const setUsername = (username: string) => dispatch(setUserName(username));
  const setPassword = (password: string) => dispatch(setUserPassword(password));

  const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === "Enter") {
      signIn();
    }
  };
  const [error, setError] = useState<boolean>(false);

  const signIn = () => {
    dispatch(requestUser());
    history.push("/");
  };

  return (
    <div className="LoginConteiner">
      {error && <Alert severity="error">Login or password incorect</Alert>}
      <div className="inputConteiner">
        <TextField
          value={loginInfo.username}
          onChange={(event: ChangeEventType) => setUsername(event.target.value)}
          label="Login"
          variant="outlined"
          onKeyPress={handleKeyPress}
        />
      </div>
      <div className="inputConteiner">
        <TextField
          value={loginInfo.password}
          onChange={(event: ChangeEventType) => setPassword(event.target.value)}
          type="password"
          label="Password"
          variant="outlined"
          onKeyPress={handleKeyPress}
        />
      </div>
      <Button onClick={() => signIn()} variant="outlined">
        SIGN IN
      </Button>
    </div>
  );
}
