import { Switch, Route } from 'react-router-dom'

import Header from "./components/Header";
import Home from "./Pages/Home"
import Login from "./Pages/Login"

export default () => (
    <>
        <Header/>
        <Switch>
            <Route component={Home} path="/" exact></Route>
            <Route component={Login} path="/Login"></Route>
        </Switch>
    </>
)