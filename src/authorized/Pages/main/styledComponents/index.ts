import styled, { css, StyledComponent, StyledFunction } from 'styled-components';
import { Box, BoxProps } from "@material-ui/core";
import { ComponentType } from 'react';


export const FlexBoxStyled = styled(Box)`
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const ContainerStyled = styled(Box)`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  height: 100%;
`;

export const AddProjectBoxStyled = styled(FlexBoxStyled)`
  height: 40px;
  justify-content: flex-end;
  padding-right: 15px;
`;

export const SearchProjectBoxStyled = styled(FlexBoxStyled)`
  height: 60px;
`;

export const TagsBoxStyled = styled(FlexBoxStyled)`
  height: 50px;
  overflow-x: scroll;
  overflow-y: hidden;
  margin: 3px;
  color: #FFFFFF;
`;

export const ProjectsBoxStyled = styled(Box)`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  height: calc(100vh - 110px);
  margin-top: 5px;
`;

export const ProjectInfoBoxStyled = styled(Box)`
  display: flex;
  flex-direction: column;
  overflow-y: auto;
  height: calc(100vh - 150px);
  margin: 5px;
`;

interface ProjectBoxProps {
  isSelected?: boolean;
};

export const ProjectBoxStyled = styled(FlexBoxStyled)<ProjectBoxProps>`
  color: ${props => props.isSelected ? '#000000' : '#FFFFFF'};
  justify-content: space-between;
  padding-left: 15px;
  padding-right: 15px;
  margin-top:10px;
  background-color: ${props => props.isSelected ? '#F7D66D;' : undefined};
`;

export const ItemBoxStyled = styled(FlexBoxStyled)<ProjectBoxProps>`
  color: ${props => props.isSelected ? '#000000' : '#FFFFFF'};
  justify-content: space-between;
  width: 80%:
  padding-left: 15px;
  padding-right: 15px;
  margin-top:10px;
  background-color: ${props => props.isSelected ? '#F7D66D;' : undefined};
`;

export const TagBoxStyled = styled(ProjectBoxStyled)`
  margin-top:0;  
  width: 211px;
`;