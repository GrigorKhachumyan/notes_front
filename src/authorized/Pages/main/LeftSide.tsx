import { useState, useEffect } from "react";
import { Box, TextField } from "@material-ui/core";
import { Delete as DeleteIcon } from "@material-ui/icons";

import { requestProject, deleteProject } from "../../../redux/sagas/actions";

import CreateProjectModal from "./components/CreateMoadl";
import DeleteProjectModal from "./components/DeleteModal";
import EditProjectModal from "./components/EditModal";
import {
  ContainerStyled,
  AddProjectBoxStyled,
  SearchProjectBoxStyled,
  ProjectsBoxStyled,
  ProjectBoxStyled,
  FlexBoxStyled,
} from "./styledComponents";
import { ChangeEventType, IProject } from "../../../types";
import { useDispatch, useSelector } from "../../../hooks";
import { setClickedProject } from "../../../redux/reducers/clickedItems/actions";

export default function LeftSide() {
  const dispatch = useDispatch();

  const projects = useSelector((state) => {
    return state.projects.projects;
  });

  const clickedProject = useSelector((state) => {
    return state.clickedItems.clikedProject;
  });

  const [projectName, setProjectName] = useState("");

  useEffect(() => {
    dispatch(requestProject());
  }, []);

  return (
    <ContainerStyled>
      <SearchProjectBoxStyled>
        <TextField
          value={projectName}
          onChange={(event: ChangeEventType) =>
            setProjectName(event.target.value)
          }
        ></TextField>
      </SearchProjectBoxStyled>
      <ProjectsBoxStyled>
        {!projects.length && <Box>No Projects</Box>}
        {projects.length &&
          projects.map((project: IProject) => (
            <ProjectBoxStyled
              isSelected={clickedProject.id === project.id}
              key={project.id}
              onClick={() => {
                dispatch(setClickedProject(project));
              }}
            >
              <Box>{project.name}</Box>
              {clickedProject.id === project.id && (
                <FlexBoxStyled>
                  <EditProjectModal
                    editingItemName="project"
                    item={project}
                  ></EditProjectModal>
                  <DeleteProjectModal
                    deletingItemName="project"
                    itemId={project.id}
                  />
                </FlexBoxStyled>
              )}
            </ProjectBoxStyled>
          ))}
      </ProjectsBoxStyled>
      <AddProjectBoxStyled>
        <CreateProjectModal creatingItemName="project" />
      </AddProjectBoxStyled>
    </ContainerStyled>
  );
}
