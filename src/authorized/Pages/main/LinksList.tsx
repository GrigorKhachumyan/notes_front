import { Box } from "@material-ui/core";
import { useEffect } from "react";
import { useDispatch, useSelector } from "../../../hooks";
import { requestLink } from "../../../redux/sagas/actions";
import { ItemBoxStyled, FlexBoxStyled, TagBoxStyled } from "./styledComponents";
import DeleteProjectModal from "./components/DeleteModal";
import EditProjectModal from "./components/EditModal";
import { setClickedLink } from "../../../redux/reducers/clickedItems/actions";

export default () => {
  const links = useSelector((state) => {
    return state.links.links;
  });

  const clickedProject = useSelector((state) => {
    return state.clickedItems.clikedProject;
  });

  const clickedLink = useSelector((state) => {
    return state.clickedItems.clikedLink;
  });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestLink(clickedProject.id));
  }, [clickedProject.id]);

  return (
    <>
      <h1>{clickedProject.name}</h1>
      <ItemBoxStyled isSelected={false}>
        <Box>link</Box>
        <Box>Edit</Box>
      </ItemBoxStyled>
      {links.map((link) => (
        <ItemBoxStyled
          key={link.id}
          isSelected={clickedLink.id === link.id}
          onClick={() => {
            dispatch(setClickedLink(link));
          }}
        >
          <FlexBoxStyled>
            <Box>{link.link}</Box>
          </FlexBoxStyled>
          {clickedLink.id === link.id && (
            <FlexBoxStyled>
              <EditProjectModal editingItemName="link" item={link} />
              <DeleteProjectModal deletingItemName="link" itemId={link.id} />
            </FlexBoxStyled>
          )}
        </ItemBoxStyled>
      ))}
    </>
  );
};
