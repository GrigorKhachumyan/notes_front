import { Box } from "@material-ui/core";
import { useEffect } from "react";
import { useDispatch, useSelector } from "../../../hooks";
import { requestInformation } from "../../../redux/sagas/actions";
import { ItemBoxStyled, FlexBoxStyled, TagBoxStyled } from "./styledComponents";

import DeleteProjectModal from "./components/DeleteModal";
import EditProjectModal from "./components/EditModal";
import { setClickedInformation } from "../../../redux/reducers/clickedItems/actions";

export default () => {
  const informations = useSelector((state) => {
    return state.informations.informations;
  });

  const clickedProject = useSelector((state) => {
    return state.clickedItems.clikedProject;
  });

  const clikedInformation = useSelector((state) => {
    return state.clickedItems.clikedInformation;
  });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestInformation(clickedProject.id));
  }, [clickedProject.id]);

  return (
    <>
      <h1>{clickedProject.name}</h1>
      <ItemBoxStyled isSelected={false}>
        <FlexBoxStyled>
          <Box>mail</Box>
          <Box>password</Box>
        </FlexBoxStyled>
        <FlexBoxStyled>
          <Box>Edit</Box>
        </FlexBoxStyled>
      </ItemBoxStyled>
      {informations.map((info) => (
        <ItemBoxStyled
          key={info.id}
          isSelected={clikedInformation.id === info.id}
          onClick={() => {
            dispatch(setClickedInformation(info));
          }}
        >
          <FlexBoxStyled>
            <Box>{info.mail}</Box>
            <Box>{info.password}</Box>
          </FlexBoxStyled>
          {clikedInformation.id === info.id && (
            <FlexBoxStyled>
              <EditProjectModal editingItemName="information" item={info} />
              <DeleteProjectModal
                deletingItemName="information"
                itemId={info.id}
              />
            </FlexBoxStyled>
          )}
        </ItemBoxStyled>
      ))}
    </>
  );
};
