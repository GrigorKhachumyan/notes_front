import { Box } from "@material-ui/core";
import {
  TagsBoxStyled,
  FlexBoxStyled,
  TagBoxStyled,
  ProjectBoxStyled,
} from "./styledComponents";

import CreateProjectModal from "./components/CreateMoadl";
import EditProjectModal from "./components/EditModal";

import { useDispatch, useSelector } from "../../../hooks";
import { setClickedTag } from "../../../redux/reducers/clickedItems/actions";

export default () => {
  const dispatch = useDispatch();

  const clickedTag = useSelector((state) => {
    return state.clickedItems.clickedTag;
  });

  return (
    <TagsBoxStyled>
      <ProjectBoxStyled
        isSelected={clickedTag === "information"}
        onClick={() => {
          dispatch(setClickedTag("information"));
        }}
      >
        <Box component="span">information</Box>
        {clickedTag === "information" && (
          <FlexBoxStyled>
            <CreateProjectModal creatingItemName="information"></CreateProjectModal>
          </FlexBoxStyled>
        )}
      </ProjectBoxStyled>
      <ProjectBoxStyled
        isSelected={clickedTag === "links"}
        onClick={() => {
          dispatch(setClickedTag("links"));
        }}
      >
        <Box component="span">links</Box>
        {clickedTag === "links" && (
          <CreateProjectModal creatingItemName="link"></CreateProjectModal>
        )}
      </ProjectBoxStyled>
    </TagsBoxStyled>
  );
};
