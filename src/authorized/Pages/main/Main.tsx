import {
  styled,
  makeStyles,
  createStyles,
  Theme,
} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

import LeftSide from "./LeftSide";
import RightSide from "./RightSide";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    item: {
      padding: 0,
    },
  })
);

const GeneralContainer = styled(Container)({
  display: "flex",
  height: "100%",
  padding: 0,
  backgroundColor: "#252525",
});

const GridItem = styled(Grid)({
  padding: 0,
});

export default () => {
  const classes = useStyles();

  return (
    <GeneralContainer maxWidth={false}>
      <Grid container>
        <Grid
          item
          xs={3}
          classes={{
            item: classes.item,
          }}
        >
          <LeftSide></LeftSide>
        </Grid>
        <GridItem item xs={9}>
          <RightSide></RightSide>
        </GridItem>
      </Grid>
    </GeneralContainer>
  );
};
