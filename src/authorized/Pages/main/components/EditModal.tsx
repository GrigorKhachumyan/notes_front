import React, { useState } from "react";
import { useDispatch } from "../../../../hooks";

import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
} from "@material-ui/core";
import { Edit as EditIcon } from "@material-ui/icons";

import {
  updateProject,
  updateInformation,
  updateLink,
} from "../../../../redux/sagas/actions";
import {
  ChangeEventType,
  IProject,
  ILink,
  IInformation,
} from "../../../../types";

type Props = { editingItemName: string; item: any }; //IProject & ILink & IInformation

export default function FormDialog(props: Props) {
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);
  const [projectName, setProjectName] = useState("");
  const [link, setLink] = useState("");
  const [infoMail, setInfoMail] = useState("");
  const [infoPassword, setInfoPassword] = useState("");

  const handleClose = () => {
    setOpen(false);
    setProjectName("");
  };

  return (
    <Box>
      <EditIcon onClick={() => setOpen(true)} />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Edit {props.editingItemName}
        </DialogTitle>
        {props.editingItemName === "project" && (
          <DialogContent>
            <TextField
              value={projectName}
              onChange={(e: ChangeEventType) => setProjectName(e.target.value)}
              autoFocus
              margin="dense"
              label="Project Name"
              type="text"
              fullWidth
            />
          </DialogContent>
        )}
        {props.editingItemName === "link" && (
          <DialogContent>
            <TextField
              value={link}
              onChange={(e: ChangeEventType) => setLink(e.target.value)}
              autoFocus
              margin="dense"
              label="Link"
              type="text"
              fullWidth
            />
          </DialogContent>
        )}
        {props.editingItemName === "information" && (
          <DialogContent>
            <TextField
              value={infoMail}
              onChange={(e: ChangeEventType) => setInfoMail(e.target.value)}
              autoFocus
              margin="dense"
              label="Mail"
              type="text"
              fullWidth
            />
            <TextField
              value={infoPassword}
              onChange={(e: ChangeEventType) => setInfoPassword(e.target.value)}
              autoFocus
              margin="dense"
              label="Password"
              type="text"
              fullWidth
            />
          </DialogContent>
        )}
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => {
              if (props.editingItemName === "project") {
                if (projectName) {
                  dispatch(
                    updateProject({ id: props.item.id, name: projectName })
                  );
                }
              } else if (props.editingItemName === "link") {
                if (link) {
                  dispatch(
                    updateLink({
                      id: props.item.id,
                      project: props.item.project,
                      link: link,
                    })
                  );
                }
              } else {
                if (infoMail && infoPassword) {
                  dispatch(
                    updateInformation({
                      id: props.item.id,
                      project: props.item.project,
                      mail: infoMail,
                      password: infoPassword,
                    })
                  );
                }
              }
              handleClose();
            }}
            color="primary"
          >
            Edit
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
