import React, { useState } from "react";
import { useDispatch } from "../../../../hooks";

import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Box,
} from "@material-ui/core";
import { Delete as DeleteIcon } from "@material-ui/icons";

import {
  deleteInformation,
  deleteLink,
  deleteProject,
} from "../../../../redux/sagas/actions";

type Props = { deletingItemName: string; itemId: number };

export default function FormDialog(props: Props) {
  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box>
      <DeleteIcon onClick={() => setOpen(true)} />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Are you sure</DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => {
              if (props.deletingItemName === "project") {
                dispatch(deleteProject(props.itemId));
              } else if (props.deletingItemName === "link") {
                dispatch(deleteLink(props.itemId));
              } else {
                dispatch(deleteInformation(props.itemId));
              }
              handleClose();
            }}
            color="primary"
          >
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
