import React, { useState } from "react";
import { useDispatch, useSelector } from "../../../../hooks";

import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
} from "@material-ui/core";
import { AddCircle as AddCircleIcon } from "@material-ui/icons";
import { ChangeEventType } from "../../../../types";

import {
  createProject,
  createInformation,
  createLink,
} from "../../../../redux/sagas/actions";

type Props = { creatingItemName: string };

export default function FormDialog(props: Props) {
  const dispatch = useDispatch();

  const clickedProject = useSelector(
    (state) => state.clickedItems.clikedProject
  );

  const [open, setOpen] = useState(false);
  const [projectName, setProjectName] = useState("");
  const [link, setLink] = useState("");
  const [infoMail, setInfoMail] = useState("");
  const [infoPassword, setInfoPassword] = useState("");

  const handleClose = () => {
    setOpen(false);
    if (props.creatingItemName === "project") {
      setProjectName("");
    } else if (props.creatingItemName === "link") {
      setLink("");
    } else {
      setInfoMail("");
      setInfoPassword("");
    }
  };

  return (
    <Box>
      <AddCircleIcon onClick={() => setOpen(true)} />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Create new {props.creatingItemName}
        </DialogTitle>
        {props.creatingItemName === "project" && (
          <DialogContent>
            <DialogContentText>Please write new project name</DialogContentText>
            <TextField
              value={projectName}
              onChange={(e: ChangeEventType) => setProjectName(e.target.value)}
              autoFocus
              margin="dense"
              label="Project Name"
              type="text"
              fullWidth
            />
          </DialogContent>
        )}
        {props.creatingItemName === "link" && (
          <DialogContent>
            <DialogContentText>Please write new link</DialogContentText>
            <TextField
              value={link}
              onChange={(e: ChangeEventType) => setLink(e.target.value)}
              autoFocus
              margin="dense"
              label="Link"
              type="text"
              fullWidth
            />
          </DialogContent>
        )}
        {props.creatingItemName === "information" && (
          <DialogContent>
            <DialogContentText>Please write new information</DialogContentText>
            <TextField
              value={infoMail}
              onChange={(e: ChangeEventType) => setInfoMail(e.target.value)}
              autoFocus
              margin="dense"
              label="Mail"
              type="text"
              fullWidth
            />
            <TextField
              value={infoPassword}
              onChange={(e: ChangeEventType) => setInfoPassword(e.target.value)}
              autoFocus
              margin="dense"
              label="Password"
              type="text"
              fullWidth
            />
          </DialogContent>
        )}
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => {
              if (props.creatingItemName === "project") {
                if (projectName) {
                  dispatch(createProject({ name: projectName }));
                }
              } else if (props.creatingItemName === "link") {
                if (link) {
                  dispatch(
                    createLink({ project: clickedProject.id, link: link })
                  );
                }
              } else {
                if (infoMail && infoPassword) {
                  dispatch(
                    createInformation({
                      project: clickedProject.id,
                      mail: infoMail,
                      password: infoPassword,
                    })
                  );
                }
              }
              handleClose();
            }}
            color="primary"
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
