import Header from "../components/Header";
import Tags from "./Tags";
import { Box } from "@material-ui/core";
import { useDispatch, useSelector } from "../../../hooks";
import { setClickedProject } from "../../../redux/reducers/clickedItems/actions";
import InformationsList from "./InformationsList";
import LinksList from "./LinksList";

import { ContainerStyled, ProjectInfoBoxStyled } from "./styledComponents";

export default () => {
  const clickedProject = useSelector((state) => {
    return state.clickedItems.clikedProject;
  });

  const clickedTag = useSelector((state) => {
    return state.clickedItems.clickedTag;
  });

  return (
    <ContainerStyled>
      <Header></Header>
      {clickedProject.id && (
        <>
          <Tags></Tags>
          <ProjectInfoBoxStyled>
            {clickedTag === "information" ? (
              <InformationsList></InformationsList>
            ) : (
              <LinksList></LinksList>
            )}
          </ProjectInfoBoxStyled>
        </>
      )}
    </ContainerStyled>
  );
};
