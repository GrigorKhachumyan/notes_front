import { Switch, Route, useHistory } from "react-router-dom";
import { connect } from "react-redux";

import { Button, Box } from "@material-ui/core";
import MeetingRoomIcon from "@material-ui/icons/MeetingRoom";

import InputHeader from "../main/components/InputHeader";

import { HeaderContainerStyled, FlexBoxStyled } from "./style";

function Header(props: any) {
  //props.type
  const history = useHistory();

  return (
    <HeaderContainerStyled>
      <Box>
        <Switch>
          <Route component={InputHeader} path="/" exact></Route>
        </Switch>
      </Box>
      <FlexBoxStyled>
        <Switch>
          <Route
            path="/"
            exact
            render={() => (
              <Button onClick={() => history.push("dashboard")}>
                Dashboard
              </Button>
            )}
          />
          <Route
            path="/dashboard"
            render={() => (
              <Button onClick={() => history.push("/")}>Main</Button>
            )}
          />
        </Switch>

        <MeetingRoomIcon
          onClick={() => {
            localStorage.setItem("auth_token", "");
            props.onSetToken(""); ///props.type
          }}
        />
      </FlexBoxStyled>
    </HeaderContainerStyled>
  );
}

function mapDispatchToProps(dispatch: any) {
  return {
    onSetToken: (token: string) =>
      dispatch({ type: "USER/SET_TOKEN_S", payload: token }),
  };
}

export default connect(null, mapDispatchToProps)(Header);
