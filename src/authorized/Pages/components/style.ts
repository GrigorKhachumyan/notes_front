import styled from 'styled-components';

import { Box } from "@material-ui/core";

export const FlexBoxStyled = styled(Box)`
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const HeaderContainerStyled = styled(FlexBoxStyled)`
    justify-content: space-between;
    background-color: #EBEBEB;
    height: 60px;
    padding-left: 15px;
    padding-right: 15px;
`;