import LeftSide from "./LeftSide";
import RightSide from "./RightSide";

export default () => (
  <div>
    <LeftSide></LeftSide>
    <RightSide></RightSide>
  </div>
);
