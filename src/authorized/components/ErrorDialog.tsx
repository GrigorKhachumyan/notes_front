import React from "react";
import { useDispatch, useSelector } from "../../hooks";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";

import { deleteError } from "../../redux/reducers/errorHandling/actions";

export default function FormDialog() {
  const dispatch = useDispatch();

  const error = useSelector((state) => {
    return state.error;
  });

  const handleClose = () => {
    dispatch(deleteError());
  };

  return (
    <Dialog
      open={error.status}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Something went wrong</DialogTitle>
      <DialogContent>
        <DialogContentText>{error.error}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          OKAY
        </Button>
      </DialogActions>
    </Dialog>
  );
}
