import { Switch, Route } from "react-router-dom";
import Main from "./Pages/main/Main";
import Dashboard from "./Pages/dashboard/Dashboard";
import ErrorDialog from "./components/ErrorDialog";

export default () => (
  <>
    <ErrorDialog />
    <Switch>
      <Route component={Main} path="/" exact></Route>
      <Route component={Dashboard} path="/Dashboard"></Route>
    </Switch>
  </>
);
