import React, { useEffect } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";

import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "./theme";

import AuthUserPage from "./authorized/AuthUserPage";
import UnAuthUserPage from "./unauthorized/UnAuthUserPage";

function App(props: any) {
  useEffect(() => {
    if (localStorage.getItem("auth_token")) {
      props.onSetToken(localStorage.getItem("auth_token"));
    } else {
      localStorage.setItem("auth_token", "");
      props.onSetToken("");
    }
  }, []);

  return (
    <>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          {props.token ? <AuthUserPage /> : <UnAuthUserPage />}
        </BrowserRouter>
      </ThemeProvider>
    </>
  );
}

function mapStateToProps(state: any) {
  return {
    token: state.user.token,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    onSetToken: (token: string) =>
      dispatch({ type: "USER/SET_TOKEN_S", payload: token }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
