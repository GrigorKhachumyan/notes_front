export const loginURL = "http://localhost:8000/api/v1/auth/login/";
export const userInfoURL = "http://localhost:8000/api/v1/auth/user_profile/";

export const projectURL = "http://localhost:8000/api/v1/project";

export const informationsURL = "http://localhost:8000/api/v1/information"; //---

export const linksURL = "http://localhost:8000/api/v1/link"; //---
