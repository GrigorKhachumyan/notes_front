import { takeEvery, put, call, select, StrictEffect } from "redux-saga/effects";

import { setUser, setToken } from "../../reducers/login/actions";
import { REQUEST_USER } from "./types";
import { loginURL, userInfoURL } from "../urls";
import { unauthorizedPost, get } from "../../../fetch";
import { IAction, IProject, IProjectReq, IUser } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(REQUEST_USER, sagaWorker);
}

function* sagaWorker(): Generator<StrictEffect, void, AxiosResponse & IUser & {token:string}> {
  let loginInfo:IUser = yield select((state) => state.user.loginInfo);
  const payloadToken = yield call(() => loginRequest(loginInfo));
  yield put(setToken(payloadToken.token));
  const payloadUserInfo = yield call(userInfoRequest);
  yield put(setUser(payloadUserInfo));
}

async function loginRequest(loginInfo:IUser): Promise<AxiosResponse>  {
  const response = await unauthorizedPost(loginURL, loginInfo);
  return response;
}

async function userInfoRequest() {
  const response = await get(userInfoURL);
  return response;
}
