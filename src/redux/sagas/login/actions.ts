import { REQUEST_USER } from "./types";
import { IAction, IProject, IInformation, ILink, IProjectReq, IInformationReq, ILinkReq, IActionType } from '../../../types'

export function requestUser():IActionType {
  return {
    type: REQUEST_USER,
  };
}
