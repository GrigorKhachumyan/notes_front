import {
  REQUEST_PROJECTS,
  CREATE_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT,
  REQUEST_INFORMATIONS,
  CREATE_INFORMATION,
  UPDATE_INFORMATION,
  DELETE_INFORMATION,
  REQUEST_LINKS,
  CREATE_LINK,
  UPDATE_LINK,
  DELETE_LINK,
} from "./types";
import { IAction, IProject, IInformation, ILink, IProjectReq, IInformationReq, ILinkReq, IActionType } from '../../types'

export function requestProject(): IActionType {
  return {
    type: REQUEST_PROJECTS,
  };
}

export function createProject(payload:IProjectReq): IAction<IProjectReq> {
  return {
    type: CREATE_PROJECT,
    payload,
  };
}

export function updateProject(payload:IProject):IAction<IProject> {
  return {
    type: UPDATE_PROJECT,
    payload,
  };
}

export function deleteProject(payload:number):IAction<number> {
  return {
    type: DELETE_PROJECT,
    payload,
  };
}

export function requestInformation(payload: number): IAction<number> {
  return {
    type: REQUEST_INFORMATIONS,
    payload,
  };
}

export function createInformation(payload:IInformationReq): IAction<IInformationReq> {
  return {
    type: CREATE_INFORMATION,
    payload,
  };
}

export function updateInformation(payload:IInformation): IAction<IInformation> {
  return {
    type: UPDATE_INFORMATION,
    payload,
  };
}

export function deleteInformation(payload:number): IAction<number> {
  return {
    type: DELETE_INFORMATION,
    payload,
  };
}

export function requestLink(payload: number): IAction<number> {
  return {
    type: REQUEST_LINKS,
    payload,
  };
}

export function createLink(payload:ILinkReq): IAction<ILinkReq> {
  return {
    type: CREATE_LINK,
    payload,
  };
}

export function updateLink(payload:ILink): IAction<ILink> {
  return {
    type: UPDATE_LINK,
    payload,
  };
}

export function deleteLink(payload:number): IAction<number> {
  return {
    type: DELETE_LINK,
    payload,
  };
}
