import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { DELETE_PROJECT } from "../types";
import { deleteProject } from "../../reducers/projects/actions";
import { projectURL } from "../urls";
import { remove } from "../../../fetch";
import { IAction} from '../../../types'
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(DELETE_PROJECT, sagaWorker);
}

function* sagaWorker(action:IAction<number>): Generator<StrictEffect, void, AxiosResponse> {
  yield call(DeleteProjectRequest, action.payload);
  yield put(deleteProject(action.payload));
}

async function DeleteProjectRequest(id:number): Promise<AxiosResponse> {
  const response = await remove(projectURL + `/${id}`);
  return response;
}
