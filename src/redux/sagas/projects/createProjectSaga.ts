import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { CREATE_PROJECT } from "../types";
import { createProject } from "../../reducers/projects/actions";
import { projectURL } from "../urls";
import { post } from "../../../fetch";
import { setError } from '../../reducers/errorHandling/actions'
import { IAction, IProject, IProjectReq } from '../../../types'
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(CREATE_PROJECT, sagaWorker);
}

function* sagaWorker(action:IAction<IProjectReq>): Generator<StrictEffect, void, AxiosResponse<IProject> & IProject> {
  const payload = yield call( createProjectRequest, action.payload);
  if (payload.status) {
    yield put(setError(payload.data.name[0]));
  } else {
    yield put(createProject(payload));
  }
}

async function createProjectRequest(data:IProjectReq): Promise<AxiosResponse<IProject>> {
  const response = await post(projectURL, data);
  return response;
}
