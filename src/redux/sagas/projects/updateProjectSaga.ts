import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { UPDATE_PROJECT } from "../types";
import { updateProject } from "../../reducers/projects/actions";
import { projectURL } from "../urls";
import { update } from "../../../fetch";
import { IAction, IProject } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(UPDATE_PROJECT, sagaWorker);
}

function* sagaWorker(action:IAction<IProject>): Generator<StrictEffect, void, AxiosResponse<IProject> & IProject> {
  yield call(() => UpdateProjectRequest(action.payload));
  yield put(updateProject(action.payload));
}

async function UpdateProjectRequest(data:IProject): Promise<AxiosResponse<IProject>> {
  const response = await update(projectURL + `/${data.id}`, {
    name: data.name,
  });
  return response;
}
