import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { REQUEST_PROJECTS } from "../types";
import { requestProject } from "../../reducers/projects/actions";
import { projectURL } from "../urls";
import { get } from "../../../fetch";
import { IProject } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(REQUEST_PROJECTS, sagaWorker);
}

function* sagaWorker(): Generator<StrictEffect, void, AxiosResponse & Array<IProject>> {
  const payload = yield call(getProjectsRequest);
  yield put(requestProject(payload));
}

async function getProjectsRequest(): Promise<AxiosResponse> {
  const response = await get(projectURL);
  return response;
}
