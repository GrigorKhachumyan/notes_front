import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { REQUEST_INFORMATIONS } from "../types";
import { requestInformation } from "../../reducers/informations/actions";
import { informationsURL } from "../urls";
import { get } from "../../../fetch";
import { IAction, IInformation } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(REQUEST_INFORMATIONS, sagaWorker);
}

function* sagaWorker(action:IAction<number>): Generator<StrictEffect, void, AxiosResponse & Array<IInformation>>  {
  const payload = yield call(getInformationsRequest, action.payload);
  yield put(requestInformation(payload));
}

async function getInformationsRequest(projectId:number): Promise<AxiosResponse> {
  const response = await get(informationsURL + "?project=" + projectId);
  return response;
}
