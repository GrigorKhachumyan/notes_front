import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { DELETE_INFORMATION } from "../types";
import { deleteInformation } from "../../reducers/informations/actions";
import { informationsURL } from "../urls";
import { remove } from "../../../fetch";
import { IAction } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(DELETE_INFORMATION, sagaWorker);
}

function* sagaWorker(action:IAction<number>): Generator<StrictEffect, void, AxiosResponse> {
  yield call(() => DeleteInformationRequest(action.payload));
  yield put(deleteInformation(action.payload));
}

async function DeleteInformationRequest(id:number): Promise<AxiosResponse> {
  const response = await remove(informationsURL + `/${id}`);
  return response;
}
