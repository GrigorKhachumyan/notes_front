import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { CREATE_INFORMATION } from "../types";
import { createInformation } from "../../reducers/informations/actions";
import { informationsURL } from "../urls";
import { post } from "../../../fetch";
import { IAction, IInformation, IInformationReq } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(CREATE_INFORMATION, sagaWorker);
}

function* sagaWorker(action:IAction<IInformationReq>): Generator<StrictEffect, void, AxiosResponse<IInformation> & IInformation> {
  const payload = yield call(() => createInformationRequest(action.payload));
  yield put(createInformation(payload));
}

async function createInformationRequest(data:IInformationReq): Promise<AxiosResponse<IInformation>> {
  const response = await post(informationsURL, data);
  return response;
}
