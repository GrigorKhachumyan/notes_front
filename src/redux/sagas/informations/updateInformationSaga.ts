import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { UPDATE_INFORMATION } from "../types";
import { updateInformation } from "../../reducers/informations/actions";
import { informationsURL } from "../urls";
import { update } from "../../../fetch";
import { IAction, IInformation } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(UPDATE_INFORMATION, sagaWorker);
}

function* sagaWorker(action:IAction<IInformation>): Generator<StrictEffect, void, AxiosResponse<IInformation> & IInformation> {
  yield call(() => UpdateInformationRequest(action.payload));
  yield put(updateInformation(action.payload));
}

async function UpdateInformationRequest(data:IInformation): Promise<AxiosResponse<IInformation>> {
  const response = await update(informationsURL + `/${data.id}`, {
    mail: data.mail,
    password: data.password
  });
  return response;
}
