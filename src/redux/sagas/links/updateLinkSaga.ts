import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { UPDATE_LINK } from "../types";
import { updateLink } from "../../reducers/links/actions";
import { linksURL } from "../urls";
import { update } from "../../../fetch";
import { IAction, ILink } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(UPDATE_LINK, sagaWorker);
}

function* sagaWorker(action:IAction<ILink>): Generator<StrictEffect, void, AxiosResponse<ILink> & ILink> {
  yield call(() => UpdateLinkRequest(action.payload));
  yield put(updateLink(action.payload));
}

async function UpdateLinkRequest(data:ILink): Promise<AxiosResponse<ILink>> {
  const response = await update(linksURL + `/${data.id}`, {
    link: data.link,
  });
  return response;
}
