import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { REQUEST_LINKS } from "../types";
import { requestLink } from "../../reducers/links/actions";
import { linksURL } from "../urls";
import { get } from "../../../fetch";
import { IAction, ILink } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(REQUEST_LINKS, sagaWorker);
}

function* sagaWorker(action:IAction<number>): Generator<StrictEffect, void, AxiosResponse & Array<ILink>>  {
  const payload = yield call(getLinksRequest, action.payload);
  yield put(requestLink(payload));
}

async function getLinksRequest(projectId:number): Promise<AxiosResponse> {
  const response = await get(linksURL + "?project=" + projectId);
  return response;
}
