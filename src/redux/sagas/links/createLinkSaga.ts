import { takeEvery, put, call, select, StrictEffect } from "redux-saga/effects";

import { CREATE_LINK } from "../types";
import { createLink } from "../../reducers/links/actions";
import { linksURL } from "../urls";
import { post } from "../../../fetch";
import { IAction, ILink, IInformationReq } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(CREATE_LINK, sagaWorker);
}

function* sagaWorker(action:IAction<IInformationReq>): Generator<StrictEffect, void, AxiosResponse<ILink> & ILink> {
  const payload = yield call(() => createLinkRequest(action.payload));
  yield put(createLink(payload));
}

async function createLinkRequest(data:IInformationReq): Promise<AxiosResponse<ILink>> {
  const response = await post(linksURL, data);
  return response;
}
