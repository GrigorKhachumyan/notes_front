import { takeEvery, put, call, StrictEffect } from "redux-saga/effects";

import { DELETE_LINK } from "../types";
import { deleteLink } from "../../reducers/links/actions";
import { linksURL } from "../urls";
import { remove } from "../../../fetch";
import { IAction } from "../../../types";
import { AxiosResponse } from "axios";

export default function* sagaWatcher() {
  yield takeEvery(DELETE_LINK, sagaWorker);
}

function* sagaWorker(action:IAction<number>): Generator<StrictEffect, void, AxiosResponse> {
  yield call(() => DeleteLinkRequest(action.payload));
  yield put(deleteLink(action.payload));
}

async function DeleteLinkRequest(id:number): Promise<AxiosResponse> {
  const response = await remove(linksURL + `/${id}`);
  return response;
}
