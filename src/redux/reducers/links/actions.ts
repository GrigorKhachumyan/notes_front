import { ILink, IAction } from './../../../types';

import {
  REQUEST_LINKS_S,
  CREATE_LINK_S,
  UPDATE_LINK_S,
  DELETE_LINK_S,
} from "./actionTypes";

export function requestLink(payload:Array<ILink>): IAction<Array<ILink>> {
  return {
    type: REQUEST_LINKS_S,
    payload,
  };
}

export function createLink(payload: ILink): IAction<ILink> {
  return {
    type: CREATE_LINK_S,
    payload,
  };
}

export function updateLink(payload: ILink): IAction<ILink> {
  return {
    type: UPDATE_LINK_S,
    payload,
  };
}

export function deleteLink(payload: number): IAction<number> {
  return {
    type: DELETE_LINK_S,
    payload,
  };
}
