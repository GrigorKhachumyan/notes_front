import { AnyAction } from 'redux';
import { ILink } from './../../../types';

import {
  REQUEST_LINKS_S,
  CREATE_LINK_S,
  UPDATE_LINK_S,
  DELETE_LINK_S,
} from "./actionTypes";

const initialState:{links:Array<ILink>} = {
  links: [],
};

export default function info(state = initialState, action: AnyAction) {
  let pseudoState:{links:Array<ILink>} = { ...state };
  switch (action.type) {
    case REQUEST_LINKS_S:
      pseudoState.links = action.payload;
      return pseudoState;
    case CREATE_LINK_S:
      pseudoState.links = [...pseudoState.links, action.payload];
      return pseudoState;
    case UPDATE_LINK_S:
      let linkUpdated = pseudoState.links.map((link) => {
        if (link.id === action.payload.id) {
          return action.payload;
        } else {
          return link;
        }
      });
      pseudoState.links = linkUpdated;
      return pseudoState;
    case DELETE_LINK_S:
      let linkDeleted = pseudoState.links.filter((link) => {
        return link.id !== action.payload.id;
      });
      pseudoState.links = linkDeleted;
      return pseudoState;
    default:
      return pseudoState;
  }
}
