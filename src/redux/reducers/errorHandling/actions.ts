import { IAction, IActionType } from './../../../types';

import { SET_ERROR_S, DELETE_ERROR_S } from "./actionTypes";

export function setError(payload:string):IAction<string> {
  return {
    type: SET_ERROR_S,
    payload,
  };
}

export function deleteError():IActionType {
  return {
    type: DELETE_ERROR_S,
  };
}
