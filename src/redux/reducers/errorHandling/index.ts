import { AnyAction } from 'redux';

import { SET_ERROR_S, DELETE_ERROR_S } from "./actionTypes";

interface IState {
  error: string;
  status: boolean;
}

const initialState: IState = {
  error: "",
  status: false,
};

export default function error(state = initialState, action:AnyAction) {
  let pseudoState: IState = { ...state };
  switch (action.type) {
    case SET_ERROR_S:
      pseudoState.error = action.payload;
      pseudoState.status = true;
      return pseudoState;
    case DELETE_ERROR_S:
      pseudoState.error = "";
      pseudoState.status = false;
      return pseudoState;
    default:
      return pseudoState;
  }
}
