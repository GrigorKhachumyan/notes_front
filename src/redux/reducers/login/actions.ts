import { IUser, IAction } from './../../../types';

import {
  SET_USER_S,
  SET_TOKEN_S,
  SET_LOGIN_USERNAME_S,
  SET_LOGIN_PASSWORD_S,
} from "./actionTypes";

export function setUser(payload:IUser): IAction<IUser> {
  return {
    type: SET_USER_S,
    payload,
  };
}

export function setToken(payload:string): IAction<string> {
  return {
    type: SET_TOKEN_S,
    payload,
  };
}

export function setUserName(payload:string): IAction<string> {
  return {
    type: SET_LOGIN_USERNAME_S,
    payload,
  };
}

export function setUserPassword(payload:string): IAction<string> {
  return {
    type: SET_LOGIN_PASSWORD_S,
    payload,
  };
}
