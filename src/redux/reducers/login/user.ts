import { AnyAction } from 'redux';

import {
  SET_USER_S,
  SET_TOKEN_S,
  SET_LOGIN_USERNAME_S,
  SET_LOGIN_PASSWORD_S,
} from "./actionTypes";

interface IState { 
  loginInfo: {
    username: string,
    password: string,
  };
  user: {
    id: number,
    username: string,
    email: string,
    first_name: string,
    last_name: string,
  };
  token: string; }

const initialState:IState = {
  loginInfo: {
    username: "Shahen",
    password: "123",
  },
  user: {
    id: 0,
    username: "",
    email: "",
    first_name: "",
    last_name: "",
  },
  token: "",
};

export default function info(state = initialState, action: AnyAction) {
  let pseudoState:IState = { ...state };
  switch (action.type) {
    case SET_USER_S:
      pseudoState.user = action.payload;
      return pseudoState;
    case SET_TOKEN_S:
      localStorage.setItem("auth_token", action.payload);
      pseudoState.token = action.payload;
      return pseudoState;
    case SET_LOGIN_USERNAME_S:
      let loginInfoName = { ...pseudoState.loginInfo };
      loginInfoName.username = action.payload;
      pseudoState.loginInfo = loginInfoName;
      return pseudoState;
    case SET_LOGIN_PASSWORD_S:
      let loginInfoPass = { ...pseudoState.loginInfo };
      loginInfoPass.password = action.payload;
      pseudoState.loginInfo = loginInfoPass;
      return pseudoState;
    default:
      return pseudoState;
  }
}
