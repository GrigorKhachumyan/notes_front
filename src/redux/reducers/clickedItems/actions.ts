import { IAction, IProject, ILink, IInformation } from './../../../types';

import { SET_CLICKED_PROJECT_S, SET_CLICKED_TAG_S, SET_CLICKED_LINK_S, SET_CLICKED_INFORMATION_S } from "./actionTypes";

export function setClickedProject(payload:IProject):IAction<IProject> {
  return {
    type: SET_CLICKED_PROJECT_S,
    payload,
  };
}

export function setClickedLink(payload:ILink):IAction<ILink> {
  return {
    type: SET_CLICKED_LINK_S,
    payload,
  };
}

export function setClickedInformation(payload:IInformation):IAction<IInformation> {
  return {
    type: SET_CLICKED_INFORMATION_S,
    payload,
  };
}

export function setClickedTag(payload:string):IAction<string> {
  return {
    type: SET_CLICKED_TAG_S,
    payload,
  };
}
