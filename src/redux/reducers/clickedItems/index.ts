import { AnyAction } from 'redux';
import { IAction, IProject, ILink, IInformation } from './../../../types';

import { SET_CLICKED_PROJECT_S, SET_CLICKED_LINK_S, SET_CLICKED_INFORMATION_S, SET_CLICKED_TAG_S } from "./actionTypes";

interface IState {
  clikedProject: IProject;
  clikedLink: ILink,
  clikedInformation: IInformation,
  clickedTag: string;
}

const initialState: IState = {
  clikedProject: {} as IProject,
  clikedLink: {} as ILink,
  clikedInformation: {} as IInformation,
  clickedTag: "information",
};

export default function info(state = initialState, action: AnyAction) {
  let pseudoState: IState = { ...state };
  switch (action.type) {
    case SET_CLICKED_PROJECT_S:
      pseudoState.clikedProject = action.payload;
      return pseudoState;
    case SET_CLICKED_LINK_S:
      pseudoState.clikedLink = action.payload;
      return pseudoState;
    case SET_CLICKED_INFORMATION_S:
      pseudoState.clikedInformation = action.payload;
      return pseudoState;
    case SET_CLICKED_TAG_S:
      pseudoState.clickedTag = action.payload;
      return pseudoState;
    default:
      return pseudoState;
  }
}
