import { AnyAction } from 'redux';
import { IProject } from './../../../types';

import {
  REQUEST_PROJECTS_S,
  CREATE_PROJECT_S,
  UPDATE_PROJECT_S,
  DELETE_PROJECT_S,
} from "./actionTypes";

const initialState:{projects:Array<IProject>} = {
  projects: [],
};

export default function info(state = initialState, action: AnyAction) {
  let pseudoState:{projects:Array<IProject>} = { ...state };
  switch (action.type) {
    case REQUEST_PROJECTS_S:
      pseudoState.projects = action.payload;
      return pseudoState;
    case CREATE_PROJECT_S:
      pseudoState.projects = [...pseudoState.projects, action.payload];
      return pseudoState;
    case UPDATE_PROJECT_S:
      let projectUpdated = pseudoState.projects.map((project) => {
        if (project.id === action.payload.id) {
          return action.payload;
        } else {
          return project;
        }
      });
      pseudoState.projects = projectUpdated;
      return pseudoState;
    case DELETE_PROJECT_S:
      let projectDeleted = pseudoState.projects.filter((project) => {
        return project.id !== action.payload;
      });
      pseudoState.projects = projectDeleted;
      return pseudoState;
    default:
      return pseudoState;
  }
}
