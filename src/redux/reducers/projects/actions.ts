import { IProject, IAction } from './../../../types';

import {
  REQUEST_PROJECTS_S,
  CREATE_PROJECT_S,
  UPDATE_PROJECT_S,
  DELETE_PROJECT_S,
} from "./actionTypes";

export function requestProject(payload:Array<IProject>): IAction<Array<IProject>> {
  return {
    type: REQUEST_PROJECTS_S,
    payload,
  };
}

export function createProject(payload: IProject): IAction<IProject> {
  return {
    type: CREATE_PROJECT_S,
    payload,
  };
}

export function updateProject(payload: IProject): IAction<IProject> {
  return {
    type: UPDATE_PROJECT_S,
    payload,
  };
}

export function deleteProject(payload: number): IAction<number> {
  return {
    type: DELETE_PROJECT_S,
    payload,
  };
}
