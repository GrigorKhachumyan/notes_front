import { IInformation, IAction } from './../../../types';

import {
  REQUEST_INFORMATIONS_S,
  CREATE_INFORMATION_S,
  UPDATE_INFORMATION_S,
  DELETE_INFORMATION_S,
} from "./actionTypes";

export function requestInformation(payload:Array<IInformation>): IAction<Array<IInformation>> {
  return {
    type: REQUEST_INFORMATIONS_S,
    payload,
  };
}

export function createInformation(payload: IInformation): IAction<IInformation> {
  return {
    type: CREATE_INFORMATION_S,
    payload,
  };
}

export function updateInformation(payload: IInformation): IAction<IInformation> {
  return {
    type: UPDATE_INFORMATION_S,
    payload,
  };
}

export function deleteInformation(payload: number): IAction<number> {
  return {
    type: DELETE_INFORMATION_S,
    payload,
  };
}
