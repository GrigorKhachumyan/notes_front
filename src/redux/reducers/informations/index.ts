import { AnyAction } from 'redux';
import { IInformation } from './../../../types';

import {
  REQUEST_INFORMATIONS_S,
  UPDATE_INFORMATION_S,
  CREATE_INFORMATION_S,
  DELETE_INFORMATION_S,
} from "./actionTypes";

const initialState:{informations:Array<IInformation>} = {
  informations: [],
};

export default function info(state = initialState, action: AnyAction) {
  let pseudoState:{informations:Array<IInformation>} = { ...state };
  switch (action.type) {
    case REQUEST_INFORMATIONS_S:
      pseudoState.informations = action.payload;
      return pseudoState;
    case CREATE_INFORMATION_S:
      pseudoState.informations = [...pseudoState.informations, action.payload];
      return pseudoState;
    case UPDATE_INFORMATION_S:
      let informationUpdated = pseudoState.informations.map((information) => {
        if (information.id === action.payload.id) {
          return action.payload;
        } else {
          return information;
        }
      });
      pseudoState.informations = informationUpdated;
      return pseudoState;
    case DELETE_INFORMATION_S:
      let informationDeleted = pseudoState.informations.filter(
        (information) => {
          return information.id !== action.payload.id;
        }
      );
      pseudoState.informations = informationDeleted;
      return pseudoState;
    default:
      return pseudoState;
  }
}
