import { fork } from "redux-saga/effects";

import getProjectsSaga from "./sagas/projects/getProjectsSaga";
import createProjectSaga from "./sagas/projects/createProjectSaga";
import deleteProjectSaga from "./sagas/projects/deleteProjectSaga";
import updateProjectSaga from "./sagas/projects/updateProjectSaga";
import getInformationsSaga from "./sagas/informations/getInformationsSaga";
import createInformationSaga from "./sagas/informations/createInformationSaga";
import deleteInformationSaga from "./sagas/informations/deleteInformationSaga";
import updateInformationSaga from "./sagas/informations/updateInformationSaga";
import getLinksSaga from "./sagas/links/getLinksSaga";
import createLinkSaga from "./sagas/links/createLinkSaga";
import deleteLinkSaga from "./sagas/links/deleteLinkSaga";
import updateLinkSaga from "./sagas/links/updateLinkSaga";
import loginSaga from "./sagas/login/loginSaga";

export default function* rootSaga() {
  yield fork(getProjectsSaga);
  yield fork(createProjectSaga);
  yield fork(deleteProjectSaga);
  yield fork(updateProjectSaga);
  yield fork(getInformationsSaga);
  yield fork(createInformationSaga);
  yield fork(deleteInformationSaga);
  yield fork(updateInformationSaga);
  yield fork(getLinksSaga);
  yield fork(createLinkSaga);
  yield fork(deleteLinkSaga);
  yield fork(updateLinkSaga);
  yield fork(loginSaga);
  // code after fork-effect
}
