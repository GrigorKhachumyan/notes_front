import { combineReducers } from "redux";

import projects from "./reducers/projects";
import links from "./reducers/links";
import informations from "./reducers/informations";
import user from "./reducers/login/user";
import error from "./reducers/errorHandling";
import clickedItems from "./reducers/clickedItems";

export default combineReducers({
  projects,
  links,
  informations,
  user,
  error,
  clickedItems,
});
