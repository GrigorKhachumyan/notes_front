import { TypedUseSelectorHook, useDispatch as useOriginalDispatch, useSelector as useOriginalSelector } from 'react-redux'
import type { RootState, AppDispatch } from '.'

export const useDispatch = () => useOriginalDispatch<AppDispatch>()
export const useSelector: TypedUseSelectorHook<RootState> = useOriginalSelector