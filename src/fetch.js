import axios from "axios";
import environment from "./env";

export function deleteAllCookies() {
  var cookies = document.cookie.split(";");
  for (var i = 0; i < cookies.length; i++) {
    var cookie = cookies[i];
    var eqPos = cookie.indexOf("=");
    var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
}

export function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

export function setCookie(name, value, days = null) {
  if (value === "" || value === null) {
    document.cookie =
      name + "=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  } else {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  }
}

axios.interceptors.response.use((res) => {
  return res.data;
});

export const authorizedGetConfig = (options = {}) => {
  return {
    ...options,
    baseURL: environment.baseUrl,
    transformRequest: [
      (data) => {
        let ret = "";
        for (let it in data) {
          ret +=
            encodeURIComponent(it) + "=" + encodeURIComponent(data[it]) + "&";
        }
        return ret;
      },
    ],
    transformResponse: [
      (data) => {
        return data;
      },
    ],
    headers: {
      Authorization: `JWT ${localStorage.getItem("auth_token")}`,
      "Content-Type": "application/json",
    },
    responseType: options.responseType || "json",
  };
};

export const authorizedPutConfig = (options = {}) => {
  return {
    baseURL: environment.baseUrl,
    transformRequest: [
      (data) => {
        return JSON.stringify(data);
      },
    ],
    transformResponse: [
      (data) => {
        return data;
      },
    ],
    headers: {
      Authorization: `JWT ${localStorage.getItem("auth_token")}`,
      "Content-Type": options.contentType || "application/json",
    },
    responseType: options.responseType || "json",
  };
};

export const authorizedPostConfig = (options = {}) => {
  return {
    baseURL: environment.baseUrl,
    headers: {
      Authorization: `JWT ${localStorage.getItem("auth_token")}`,
      "Content-Type": options.contentType || "application/json",
    },
    responseType: options.responseType || "json",
  };
};

export let unauthorizedPostConfig = {
  baseURL: environment.baseUrl,
  transformRequest: [
    (data) => {
      return JSON.stringify(data);
    },
  ],
  transformResponse: [
    (data) => {
      return data;
    },
  ],
  headers: {
    "Content-Type": "application/json",
  },
  responseType: "json",
};

export let unauthorizedGetConfig = {
  baseURL: environment.baseUrl,
  method: "get",
  withCredentials: false,
  headers: {
    "Content-Type": "application/json",
  },
  responseType: "json",
};

export const get = (url, options = {}) => {
  const configs = authorizedGetConfig(options);
  return axios.get(url, configs).catch((result) => {
    if (result.response.status === 401) {
      localStorage.setItem("auth_token", "");
      window.location = "/";
    }
    return result.response;
  });
};

export const post = (url, data, options = {}) => {
  const configs = authorizedPostConfig(options);
  return axios.post(url, data, configs).catch((result) => {
    if (result.response.status === 401) {
      localStorage.setItem("auth_token", "");
      window.location = "/";
    }
    return result.response;
  });
};

export const put = (url, data, options = {}) => {
  const configs = authorizedPutConfig(options);

  return axios.put(url, data, configs).catch((result) => {
    if (result.response.status === 401) {
      localStorage.setItem("auth_token", "");
      window.location = "/";
    }
    return result.response;
  });
};

export const update = (url, data) => {
  const configs = authorizedPostConfig();
  return axios.put(url, data, configs).catch((result) => {
    if (result.response.status === 401) {
      localStorage.setItem("auth_token", "");
      window.location = "/";
    }
    return result.response;
  });
};
export const remove = (url) => {
  const configs = authorizedPostConfig();
  return axios.delete(url, configs).catch((result) => {
    if (result.response.status === 401) {
      localStorage.setItem("auth_token", "");
      window.location = "/";
    }
    return result.response;
  });
};

export const unauthorizedGet = (url, options = {}) => {
  let configs = { ...unauthorizedGetConfig };
  configs = { ...configs, headers: { ...configs.headers, ...options.headers } };
  return axios.get(url, configs);
};

export const unauthorizedPost = (url, data) => {
  return axios.post(url, data, unauthorizedPostConfig);
};
