import React from "react";
import ReactDOM from "react-dom";
import { createStore, compose, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import createSagaMiddlewere from "redux-saga";
import reportWebVitals from "./reportWebVitals";
import rootSaga from "./redux/rootSaga";

import "./index.css";
import App from "./App";
import rootReducer from "./redux/rootReducer";

const saga = createSagaMiddlewere();

const store = createStore(rootReducer, compose(applyMiddleware(saga)));

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

saga.run(rootSaga);

const app = (
  <Provider store={store}>
    <App></App>
  </Provider>
);

ReactDOM.render(
  <React.StrictMode>{app}</React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
